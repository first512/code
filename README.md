# Available Scripts

In the project directory, you can run:

## Intalling

- run the following commands in terminal to install dependencies
  ```
  npm install
  ```

## start

- Run the app in development mode
  ```
  npm start
  ```
  webpack will automatically open [http://localhost:3000](http://localhost:3000) in the browser.

## dev

- Run both server and frontend at the same time

  ```
  npm run dev
  ```

## build

- Build the app in production mode
  ```
  npm run build
  ```

# Assumptions

## New features to add?

> Some UI presents in summary view, while no related features are described in the application requirements. Does that mean we don't need to handle the features as following: pagination, search, and sorting?

I have finished some reusable components for these UI. Check it out in the src/components

## Serve the data from server?

> The Application Requirements describes that the application will use a static sample-data.json file with randomly generated sample data create a simple one-page app. While the technical requirements want us to use ajax call to ExpressJS. I am not sure which way you want to see serve the data.

I have created a simple server api to serve the data from server.
