const fs = require("fs");
const path = require("path");
const express = require("express");
const app = express();
const port = process.env.PORT || 8000;

const sampleDataFilePath = path.join(__dirname, "sample-data.json");

app.listen(port, () => console.log(`Listening on port ${port}`));

app.get("/api", (req, res) => {
  const data = fs.readFileSync(sampleDataFilePath);

  res.json(JSON.parse(data));
});
