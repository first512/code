import React from "react";
import { Provider } from "react-redux";
import store from "./store";
import Company from "./views/Company";
function App() {
  return (
    <Provider store={store}>
      <Company />
    </Provider>
  );
}

export default App;
