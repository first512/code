import React, { useEffect } from "react";
import { createSelector } from "@reduxjs/toolkit";
import Header from "./Header";
import Employees from "./Employees";
import { useDispatch, useSelector } from "src/store";
import { fetchCompanyData } from "src/slices/company";

const companySelector = (state) => state.company;

const Company = () => {
  const dispatch = useDispatch();
  const { isLoading, status } = useSelector(
    createSelector(companySelector, (company) => company)
  );
  useEffect(() => {
    if (status === "idle") {
      dispatch(fetchCompanyData());
    }
  }, [status]);

  if (isLoading) return <div className="loading">Loading...</div>;

  return (
    <div className="company">
      <Header />
      <Employees />
    </div>
  );
};

export default Company;
