import React, { memo } from "react";
import { createSelector } from "@reduxjs/toolkit";
import { useSelector } from "src/store";
const companySelector = (state) => state.company;
const Header = memo(() => {
  const companyInfo = useSelector(
    createSelector(companySelector, (company) => company.companyInfo)
  );
  return (
    <div className="company__header">
      <div className="company__header__company-name">
        {companyInfo.companyName}
      </div>
      <div className="company__header__motto-est">
        <span className="company__header__motto-est__item">
          {companyInfo.companyMotto}
        </span>
        <span className="company__header__motto-est__item">
          {`Since (${new Date(companyInfo.companyEst).toLocaleDateString()})`}
        </span>
      </div>
    </div>
  );
});

export default Header;
