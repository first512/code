import React, { memo } from "react";

const Search = memo(({ value = "" }) => {
  return (
    <div className="company__employees__search">
      <input
        type="text"
        className="company__employees__search__input"
        value={value}
        placeholder=""
        onChange={() => {}}
      />
      <button className="company__employees__search__button">Search</button>
    </div>
  );
});

export default Search;
