import React, { memo } from "react";

const EmployeeDetails = memo(({ selectedEmployee }) => {
  const { avatar, jobTitle, age, dateJoined, firstName, lastName, bio } =
    selectedEmployee || {};
  return (
    <>
      {selectedEmployee && (
        <div className="company__employees__employee">
          <div className="company__employees__employee__left">
            <img
              className="company__employees__employee__left__image"
              src={avatar}
            />
            <div className="company__employees__employee__left__job-title">
              {jobTitle}
            </div>
            <div className="company__employees__employee__left__age">
              Age:{age}
            </div>
            <div className="company__employees__employee__left__date-joined">
              Joined date:{new Date(dateJoined).toLocaleDateString()}
            </div>
          </div>
          <div className="company__employees__employee__right">
            <div className="company__employees__employee__right__name">{`${firstName} ${lastName}`}</div>
            <p className="company__employees__employee__right__bio"> {bio}</p>
          </div>
        </div>
      )}
    </>
  );
});

export default EmployeeDetails;
