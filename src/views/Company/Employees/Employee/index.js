import React, { useMemo, useState, memo, useCallback } from "react";
import { showEmployeeDetails } from "src/slices/company";
import { useDispatch } from "src/store";
import Name from "./Name";
const Employee = memo(({ employee, isSelected }) => {
  const dispatch = useDispatch();
  const handleShowEmployeeDetails = useCallback(() => {
    dispatch(showEmployeeDetails({ open: true, employee }));
  }, [employee]);
  return (
    <tr
      onClick={handleShowEmployeeDetails}
      className={`${
        isSelected
          ? "company__employees__table__row-active"
          : "company__employees__table__row"
      }`}
    >
      <td className="company__employees__table__data">{employee.id}</td>
      <td className="company__employees__table__data">
        <Name
          firstName={employee.firstName}
          lastName={employee.lastName}
          avatar={employee.avatar}
        />
      </td>
      <td className="company__employees__table__data">{employee.contactNo}</td>
      <td className="company__employees__table__data">{employee.address}</td>
    </tr>
  );
});

export default Employee;
