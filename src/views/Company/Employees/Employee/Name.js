import React, { memo } from "react";
import ImageContainer from "src/components/Image";

const Name = memo(({ avatar, firstName, lastName }) => {
  return (
    <div className="company__employees__name">
      <div className="company__employees__name__avatar">
        <ImageContainer imageUrl={avatar} />
      </div>
      <div>{`${firstName} ${lastName}`}</div>
    </div>
  );
});

export default Name;
