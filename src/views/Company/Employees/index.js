import React, { useCallback, memo, useState, useMemo } from "react";
import { createSelector } from "@reduxjs/toolkit";
import Modal from "src/components/Dialog";
import SortingArrow from "src/components/Sorting";
import Pagination from "src/components/Pagination";
import { closeEmployeeDetails } from "src/slices/company";
import { useDispatch, useSelector } from "src/store";
import Search from "./Search";
import PageCount from "./PageCount";
import Employee from "./Employee";
import EmployeeDetails from "./Employee/Details";
const headers = [
  { id: "id", title: "ID", isSortable: false },
  { id: "name", title: "Name", isSortable: true },
  { id: "contactNo", title: "Contact No", isSortable: true },
  { id: "address", title: "Address", isSortable: true },
];
const PageSize = 10;
const companySelector = (state) => state.company;
const Main = memo(() => {
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(1);
  const { open, selectedEmployee } = useSelector(
    createSelector(companySelector, (company) => company.modal)
  );
  const employees = useSelector(
    createSelector(companySelector, (company) => company.employees)
  );
  const handleCloseEmployDetails = useCallback(() => {
    dispatch(closeEmployeeDetails({ open: false }));
  }, []);

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * PageSize;
    const lastPageIndex = firstPageIndex + PageSize;
    return employees.slice(firstPageIndex, lastPageIndex);
  }, [currentPage]);
  return (
    <div className="company__employees">
      <Search />
      <PageCount
        currentPage={currentPage}
        totalCount={employees.length}
        currentCount={currentTableData.length}
        pageSize={PageSize}
      />
      <div className="company__employees__table-container">
        <table className="company__employees__table">
          <thead>
            <tr>
              {headers.map((header) => (
                <th
                  key={header.id}
                  className="company__employees__table__title"
                >
                  <div className="company__employees__table__title__wrapper">
                    <span className="company__employees__table__title__text">
                      {header.title}
                    </span>
                    <SortingArrow />
                  </div>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {currentTableData.map((employee) => (
              <Employee
                employee={employee}
                key={employee.id}
                isSelected={
                  selectedEmployee && selectedEmployee.id === employee.id
                }
              />
            ))}
          </tbody>
        </table>
      </div>
      <div className="company__employees__pagination">
        <Pagination
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={employees.length}
          pageSize={PageSize}
          onPageChange={(page) => setCurrentPage(page)}
        />
      </div>
      {useMemo(
        () => (
          <Modal onClose={handleCloseEmployDetails} open={open}>
            <EmployeeDetails selectedEmployee={selectedEmployee} />
          </Modal>
        ),
        [open, selectedEmployee]
      )}
    </div>
  );
});

export default Main;
