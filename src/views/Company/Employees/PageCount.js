import React, { memo } from "react";

const PageCount = memo(
  ({ currentPage, totalCount, currentCount, pageSize }) => {
    return (
      <div className="company__employees__count">
        Showing {(currentPage - 1) * pageSize + currentCount} of {totalCount}
      </div>
    );
  }
);

export default PageCount;
