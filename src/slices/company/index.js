import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import sampleData from "../../../sample-data.json";
const initialState = {
  isLoading: false,
  status: "idle",
  companyInfo: {},
  employees: [],
  modal: {
    open: false,
    selectedEmployee: null,
  },
};
const fetchData = async () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(sampleData);
    }, 2000);
  });
};
export const fetchCompanyData = createAsyncThunk(
  "company/fetchCompanyData",
  async (undefined, { dispatch, reject }) => {
    try {
      const data = await fetchData();
      dispatch(fullfilCompanyData(data));
    } catch (error) {
      console.log(error);
    }
  }
);
const companySlice = createSlice({
  name: "company",
  initialState,
  reducers: {
    fullfilCompanyData: (state, action) => {
      const { companyInfo, employees } = action.payload;
      state.companyInfo = companyInfo;
      state.employees = employees;
    },
    showEmployeeDetails: (state, action) => {
      const { employee, open } = action.payload;
      state.modal.open = open;
      state.modal.selectedEmployee = employee;
    },
    closeEmployeeDetails: (state, action) => {
      const { open } = action.payload;
      state.modal.open = open;
      state.modal.selectedEmployee = null;
    },
  },
  extraReducers: {
    [fetchCompanyData.pending]: (state, action) => {
      state.isLoading = true;
      state.status = "pending";
    },
    [fetchCompanyData.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.status = "fulfilled";
    },
    [fetchCompanyData.rejected]: (state, action) => {
      state.isLoading = false;
      state.status = "rejected";
    },
  },
});

export const { fullfilCompanyData, showEmployeeDetails, closeEmployeeDetails } =
  companySlice.actions;
export default companySlice.reducer;
