import React from "react";
import ReactDOM from "react-dom";
import App from "./app";
import "../assets/styles/main.scss";

ReactDOM.render(React.createElement(App), document.getElementById("app"));
