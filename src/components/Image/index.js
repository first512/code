import React, { memo } from "react";

const ImageContainer = memo(({ imageUrl }) => {
  return <img src={imageUrl} className="image" />;
});

export default ImageContainer;
