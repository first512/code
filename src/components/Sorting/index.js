import React, { memo } from "react";

const SortingArrow = memo(({ sorting }) => {
  return (
    <div className="sorting">
      {sorting ? (
        sorting === "descending" ? (
          <>
            <span className="sorting__arrow-down"></span>
          </>
        ) : (
          <>
            <span className="sorting__arrow-up"></span>
          </>
        )
      ) : (
        <>
          <span className="sorting__arrow-up"></span>
          <span className="sorting__arrow-down"></span>
        </>
      )}
    </div>
  );
});

export default SortingArrow;
