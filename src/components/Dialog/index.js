import React from "react";

import Dialog from "./dialog";
class Modal extends React.PureComponent {
  /* rendor modal top */
  renderTop = () => {
    const { title, onClose } = this.props;
    return (
      <div className="modal_top">
        {title && <p>{title}</p>}
        {onClose && (
          <span
            className="modal_top__close"
            onClick={() => onClose && onClose()}
          >
            &times;
          </span>
        )}
      </div>
    );
  };

  /* render modal content */
  renderContent = () => {
    const { content, children } = this.props;
    return React.isValidElement(content) ? content : children ? children : null;
  };
  render() {
    const { open, closeCb, onClose } = this.props;
    return (
      <Dialog closeCb={closeCb} onClose={onClose} open={open}>
        {this.renderTop()}
        {this.renderContent()}
      </Dialog>
    );
  }
}
export default Modal;
