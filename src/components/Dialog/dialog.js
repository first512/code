import React, { useMemo, useEffect, useState, useRef } from "react";
import ReactDOM from "react-dom";

const controlShow = (f1, f2, value, timer) => {
  f1(value);
  return setTimeout(() => {
    f2(value);
  }, timer);
};
export default function Dialog(props) {
  const { open, closeCb, onClose } = props;
  const [modelShow, setModelShow] = useState(open);
  const [modelShowAync, setModelShowAync] = useState(open);
  const ref = useRef(null);

  const renderChildren = useMemo(() => {
    return ReactDOM.createPortal(
      <div style={{ display: modelShow ? "block" : "none" }}>
        <div
          className="modal_container"
          style={{ opacity: modelShowAync ? 1 : 0 }}
        >
          <div className="modal_wrap" ref={ref}>
            <div className="modal_wrap__children"> {props.children} </div>
          </div>
        </div>
        <div
          className="modal_container mast"
          onClick={() => onClose && onClose()}
          style={{ opacity: modelShowAync ? 0.4 : 0 }}
        />
      </div>,
      document.body
    );
  }, [modelShowAync, modelShow, ref]);

  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      open && onClose && onClose();
    }
  };
  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  }, [open]);
  useEffect(() => {
    let timer;
    if (open) {
      timer = controlShow(setModelShow, setModelShowAync, open, 30);
    } else {
      timer = controlShow(setModelShowAync, setModelShow, open, 1000);
    }
    return function () {
      timer && clearTimeout(timer);
    };
  }, [open]);
  useEffect(() => {
    !modelShow && typeof closeCb === "function" && closeCb();
  }, [modelShow]);
  return renderChildren;
}
