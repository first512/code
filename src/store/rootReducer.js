import { combineReducers } from "@reduxjs/toolkit";
import companyReducer from "src/slices/company";

const rootReducer = combineReducers({
  company: companyReducer,
});

export default rootReducer;
