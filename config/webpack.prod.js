const TerserPlugin = require("terser-webpack-plugin");
const proConfig = {
  mode: "production",
  optimization: {
    usedExports: true, // production
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
        extractComments: false,
        terserOptions: {
          compress: {
            arguments: false,
            dead_code: true,
          },
          mangle: true,
          toplevel: true,
          keep_classnames: true,
          keep_fnames: true,
        },
      }),
    ],
  },
};

module.exports = proConfig;
