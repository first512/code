const webpack = require("webpack");

const devConfig = {
  devtool: "eval",
  mode: "development",
  devServer: {
    open: true,
    hot: true,
    compress: true,
    historyApiFallback: true,
    port: 3000,
    proxy: {
      "/api": {
        target: "http://localhost:8000",
      },
    },
  },
  // plugins: [new webpack.HotModuleReplacementPlugin()],
};

module.exports = devConfig;
